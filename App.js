import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table } from 'flowbite-react';
import './App.css';

const App = () => {
  const [data, setData] = useState(null);

  const [input, setInput] = useState({
    name: ""
  });

  const [fetchStatus, setFetchStatus] = useState(true);
  const [currentId, setCurrentId] = useState(-1);

  const getCondition = (score) => {
    if (score >= 80) {
      return 'A';
    } else if (score >= 70) {
      return 'B';
    } else if (score >= 60) {
      return 'C';
    } else if (score >= 50) {
      return 'D';
    } else {
      return 'E';
    }
  };

  const handleIndexScore = (score) => {
    console.log(`Condition: ${getCondition(score)}`);
  };

  useEffect(() => {
    if (fetchStatus === true) {
      axios.get("https://backendexample.sanbercloud.com/api/contestants")
        .then((res) => {
          setData([...res.data]);
        })
        .catch((error) => {
          console.error("Error fetching data:", error);
        });
      setFetchStatus(false);
    }
  }, [fetchStatus]);

  const handleInput = (event) => {
    let name = event.target.name;
    let value = event.target.value;

    setInput({ ...input, [name]: value });
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    let { name } = input;

    if (currentId === -1) {
      axios.post('https://backendexample.sanbercloud.com/api/contestants', { name })
        .then((res) => {
          console.log(res);
          setFetchStatus(true);
        });
    } else {
      axios.put(`https://backendexample.sanbercloud.com/api/contestants/${currentId}`, { name })
        .then((res) => {
          setFetchStatus(true);
        });
    }

    setCurrentId(-1);

    setInput({ name: "" });
  };

  const handleDelete = (id) => {
    axios.delete(`https://backendexample.sanbercloud.com/api/contestants/${id}`)
      .then(() => {
        setFetchStatus(true);
      });
  };

  const handleEdit = (id) => {
    setCurrentId(id);

    axios.get(`https://backendexample.sanbercloud.com/api/contestants/${id}`)
      .then((res) => {
        let data = res.data;

        setInput({ name: data.name });
      });
  };

  return (
    <>
      <div className="overflow-x-auto">
        <Table >
          <Table.Head>
            <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>name</Table.HeadCell>
            <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>score</Table.HeadCell >
            <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>Condition</Table.HeadCell>
            <Table.HeadCell style={{ background: '#3498db', color: '#ffffff', fontFamily: 'Arial', fontWeight: 'bold', fontSize: '16px', border: '1px solid #ffffff' }}>Action</Table.HeadCell>
          </Table.Head>
          <Table.Body className="divide-y">
            {data !== null && data.map((res) => (
              <Table.Row key={res.id}>
                <Table.Cell style={{ border: '1px solid #dddddd' }}>{res.name}</Table.Cell>
                <Table.Cell style={{ border: '1px solid #dddddd' }}>{res.score}</Table.Cell>
                <Table.Cell style={{ border: '1px solid #dddddd' }}>{getCondition(res.score)}</Table.Cell>
                <Table.Cell>
                <button onClick={() => handleDelete(res.id)} type="button" class="focus:outline-none text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-900">Delete</button>
                <button onClick={() => handleEdit(res.id)} type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 me-2 mb-2 dark:bg-blue-600 dark:hover:bg-blue-700 focus:outline-none dark:focus:ring-blue-800">Edit</button>
                  
                </Table.Cell>
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      </div>
      <p>FORM DATA</p>
      <form onSubmit={handleSubmit}>
        <span>Nama : </span>
        <input onChange={handleInput} value={input.name} name='name' />
        <input type={'submit'} />
      </form>
    </>
  );
};

export default App;
